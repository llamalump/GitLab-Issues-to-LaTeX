# Compiles a string to pass to requests.get()
# Example requestURL (after concatenation):
# Where:    domain = https://gitlab.com/api/v3/projects
#        projectID = 1234567
#            query = /issues?label=issueLabel
#
# Concatenated URL:
# https://gitlab.com/api/v3/projects/1234567/issues?label=issueLabel
class URL_Maker:
    def makeRequestURL():
        # Requests user input for the domain of the URL and stores it in the
        # 'domain' variable for concatenation later on
        # i.e. https://gitlab.com/api/v3/
        print("Please enter a domain to query from (i.e. https://gitlab.com/api/v3/)")
        domain = '''console input'''

        # Requests user input for the projectID of the URL and stores it in
        # the 'projectID' variable for concatenation later on
        # i.e. 1234567
        print("Please enter a projectID (i.e. 1234567), or press ENTER if you wish to simpy query the domain you specified previously")
        projectID = '''console input'''

        # Requests user input for the query of the URL and stores it in the
        # 'query' variable for concatenation later on
        # i.e. /?labels=labelName
        print("Please enter a query (i.e. /?labels=labelName), or press ENTER if you wish to simply query the domain/projectID you specified previously")
        query = '''console input'''

    def makeRequestURL(domain):
        requestURL = domain

    def makeRequestURL(domain, projectID):
        requestURL = domain + projectID

    def makeRequestURL(domain, projectID, query):
        requestURL = domain + projectID + query
